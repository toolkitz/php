ARG BASE_IMAGE
FROM ${BASE_IMAGE} AS base
WORKDIR /app/backend
RUN apk add --update --no-cache \
        mysql-client \
        nginx \
        supervisor \
        msmtp \
        gettext  \
        py3-pip \
        libmagic \
        ca-certificates \
        inotify-tools \
        libzip-dev \
        apache2-utils \
        fcgi
RUN rm -rf /etc/nginx/conf.d/*
RUN python3 -m venv /s3cmd && \
        . /s3cmd/bin/activate && \
        pip3 install --no-cache-dir \
                s3cmd && \
        ln -s /s3cmd/bin/s3cmd /usr/local/bin/s3cmd
# RUN curl -o /usr/local/bin/php-fpm-healthcheck \
#     https://raw.githubusercontent.com/renatomefi/php-fpm-healthcheck/master/php-fpm-healthcheck \
#     && chmod +x /usr/local/bin/php-fpm-healthcheck
RUN curl -sSLf \
        -o /usr/local/bin/install-php-extensions \
        https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions && \
    chmod +x /usr/local/bin/install-php-extensions
