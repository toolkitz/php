ARG BASE_IMAGE
FROM ${BASE_IMAGE}
RUN install-php-extensions \
        gd \
        mysqli \
        opcache \
        soap \
        zip
