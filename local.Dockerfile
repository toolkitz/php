ARG BASE_IMAGE
FROM ${BASE_IMAGE} AS local
ARG BACKEND
ARG ENV
ENV BACKEND=${BACKEND}
ENV ENV=${ENV}
RUN apk add --no-cache \
    groff \
    shadow \
    vim \
    && usermod -u 1000 www-data \
    && groupmod -g 1000 www-data \
    && apk del shadow
RUN install-php-extensions xdebug
COPY base/ /
COPY local /

