# PHP Base image build

## How to add new php version?

In `.gitlab-ci.yml` in key `php-7.3-base: / parallel: / matrix: / [0] / PHP_VERSION` add to array (`[]`) new tag from [docker hub](https://hub.docker.com/_/php?tab=tags&page=1&ordering=last_updated)

## What images are build?

Images are build in following sequence:

* 1 base image for each php version (which is also base for `custom` type of image)
* 1 additional base image for each additional backend type (currently: `wordpress`, `presta`) and for each php version (so for e.g: 2 php versions x 2 backend = 4 images)
* 1 additional base image for each environment type (currently: `local`, `remote`) and for each backend type (currently: `wordpress`, `presta`, `custom`) and for each php version

## How is cache build

there are two types of build:

* scheduled -> when everything is build without cache once a week
* push / web -> triggered on `git push` or from `ui` where cache from previous versions is used to speed up build.

## How files are added to images?

* `base` folder is add to all images
* `custom` `local` `presta` `wordpress` are added to correct backend type and environment type based on the name of folder

For each folder content of the directory is added to `/` of file system. So in fact `/usr/local/bin/dumper` comes from `base/usr/local/bin/dumper`. In that way you can modify image file system without modyfing Dockerfile
