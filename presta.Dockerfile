ARG BASE_IMAGE
FROM ${BASE_IMAGE}
# https://devdocs.prestashop.com/1.7/basics/installation/system-requirements/
RUN install-php-extensions \
        ioncube_loader \
        mcrypt \
        gd \
        iconv \
        intl \
        fileinfo \
        mbstring \
        opcache \
        pdo_mysql \
        soap \
        zip \
        mysqli